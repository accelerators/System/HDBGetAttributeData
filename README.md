# HdbGetAttributeData

## Description
This project provides a Java class which can be used in a command line tool and is able to display HDB++ extracted data 
for a given attribute and period.

## Usage
At the ESRF, you can use the provided hdb_get_attribute_data.
If you are outside the ESRF, you should adapt this script to your needs.

The script (and Java class) is expecting 2 or 3 arguments.

`hdb_get_attribute_data <attribute_name> "<start_timestamp>" "<end_timestamp>"`
                       or
`hdb_get_attribute_data <attribute_name> <period_in_sec>`

### Examples
To retrieve and display data between these 2 timestamps:

`hdb_get_attribute_data tango://acudebian.esrf.fr:10000/sr/d-ct/1/current "09/07/2015 12:00:00" "10/07/2015 12:00:00"`
 
To retrieve and display the data from the last hour:

`hdb_get_attribute_data tango://acudebian.esrf.fr:10000/sr/d-ct/1/current 3600`
           
## Limitations
For the moment, this tool is only able to display data for scalar Read Only attribute but could be used as a good base 
to build a more generic tool.
The timestamps are currently displayed with a second resolution.
This could be improved in the future since HDB++ is supporting timestamps with a microsecond resolution
The errors stored in HDB++ are not displayed. An option could be added in the future to enable the display of the 
errors.
It is just a line to comment to enable the display of errors.
