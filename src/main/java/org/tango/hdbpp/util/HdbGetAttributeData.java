// Copyright (C) :      2017-2019
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
package org.tango.hdbpp.util;

import org.tango.jhdb.Hdb;
import org.tango.jhdb.HdbFailed;
import org.tango.jhdb.data.HdbData;
import org.tango.jhdb.data.HdbDataSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * HdbGetAttributeData
 * @author bourtemb
 */
public class HdbGetAttributeData
{
    public static void main( String[] args )
    {
        if(args.length < 2)
        {
            System.err.println("Expecting 2 or 3 arguments: ");
            System.err.println("                             <attribute_name> \"<start_timestamp>\" \"<end_timestamp>\"");
            System.err.println("                             or");
            System.err.println("                             <attribute_name> <period_in_sec>");
            System.err.println("E.g.: tango://acs.esrf.fr:10000/infra/a-gwater/piezo/value \"09/07/2015 12:00:00\" \"10/07/2015 12:00:00\"");
            System.err.println("E.g.: tango://acs.esrf.fr:10000/infra/a-gwater/piezo/value 3600  - to retrieve the data from the last hour");
            return;
        }
        String attr_name = args[0];
        long period = 0;
        String start_timestamp = "";
        String end_timestamp = "";

        if(args.length == 2)
        {
            period = Long.parseLong(args[1]);
            // System.out.println("period = " + period + "s");
            long now = System.currentTimeMillis();
            Date start_date = new Date(now - period * 1000);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            start_timestamp = df.format(start_date);
            end_timestamp = df.format(now);
        }
        else
        {
            start_timestamp = args[1];
            end_timestamp = args[2];
        }

        Hdb hdb = new Hdb();

        try {

            // Connect to HDB ++ using system variables
            hdb.connect();

            // Fetch data
            HdbDataSet data = hdb.getReader().getData(attr_name,start_timestamp,end_timestamp);

            // Display data
            for(int i=0;i<data.size();i++) {
                HdbData d = data.get(i);
                long ms = d.getDataTime()/1000;
                if(d.hasFailed())
                {
                    // System.out.println(Hdb.hdbDateFormat.format(ms)+" "+d.getErrorMessage());
                }
                else
                {
                    System.out.println(Hdb.hdbDateFormat.format(ms) + "\t" + d.getValueAsDouble());
                }
            }
        } catch (HdbFailed e) {
            System.out.println(e.getMessage());
        }
        hdb.disconnect();
        return;
    }
}
