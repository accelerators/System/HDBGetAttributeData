# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2] - 2020-11-02
* Recompilation in order to be able to use the latest version of HDB++.jar

## [1.1] - 2019-07-26

### Fixed
* Fix issue with quotes when forwarding the arguments passed to the script to java

### Changed
* Update startup script to use PostgreSQL DB
* Update usage example

## [1.0]
* Cassandra version
